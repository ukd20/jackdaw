create or replace PACKAGE trg_sup
AS
  PROCEDURE set_start_stop_date
  ( new_live IN VARCHAR2
  , old_live IN VARCHAR2
  , new_startdate IN OUT DATE
  , new_stopdate IN OUT DATE
  );
   -- called from BEFORE triggers. Maintains startdate and stopdate.
  PROCEDURE check_review_date
  ( new_live IN VARCHAR2
  , old_live IN VARCHAR2
  , new_reviewdate IN OUT DATE
  , message_description IN VARCHAR2
  );
   -- called from BEFORE triggers. Ensures reviewdate is not
   -- in the future for cancelled things. No longer clears
   -- reviewdate ever.
  PROCEDURE check_lock_date
  ( new_live IN VARCHAR2
  , old_live IN VARCHAR2
  , new_lockdate IN OUT DATE
  , message_description IN VARCHAR2
  );
   -- called from BEFORE triggers. Ensures lockdate is not
   -- in the future for non-cancelled things.
END;
/

REM pers has fourtypes of date
REM startdate  - When created - first insert
REM stopdate   - When cancelled  
REM reviewdate - Finishing date
REM lockdate   - done at the database level - on what basis ?

create or replace PACKAGE BODY trg_sup
AS
  PROCEDURE set_start_stop_date
  ( new_live IN VARCHAR2
  , old_live IN VARCHAR2
  , new_startdate IN OUT DATE
  , new_stopdate IN OUT DATE
  ) IS
  BEGIN
    -- startdate refers to account creation date - insert only
    IF old_live IS NULL /*inserting*/ THEN new_startdate := SYSDATE; END IF;
    -- Nothing has changed  Y->Y or N->N
    IF new_live = old_live THEN NULL; 
    -- Y->N Live account to NOT Live account stopdate should be now
    -- or
    -- N->Y NOT Live to Live stopdate should be NULL
    ELSE
      IF    new_live = 'N' THEN new_stopdate := SYSDATE;
      ELSIF new_live = 'Y' THEN new_stopdate := NULL;
      END IF;
    END IF;
  END;

  PROCEDURE check_review_date
  ( new_live IN VARCHAR2
  , old_live IN VARCHAR2
  , new_reviewdate IN OUT DATE
  , message_description IN VARCHAR2
  ) IS
  BEGIN
    -- Received Cancel Request with a requested review date in the future
    -- In IF it is Finishing
    IF new_live = 'N' AND new_reviewdate > SYSDATE THEN
      -- Account is still live and review date is set in the future
      -- Obviously Account should not be cancelled.
      IF old_live = 'Y' THEN
        raise_application_error(-20201,
          message_description ||
          ' has future reviewdate ' ||
          to_char(new_reviewdate, 'DD-Mon-YYYY') ||
          ', so cannot cancel');
      ELSE
        -- Account is not Live so no point setting a review date.
        raise_application_error(-20201,
          message_description ||
          ' is cancelled, so cannot set future reviewdate ' ||
          to_char(new_reviewdate, 'DD-Mon-YYYY'));
      END IF;
    END IF;
  END;

  PROCEDURE check_lock_date
  ( new_live IN VARCHAR2
  , old_live IN VARCHAR2
  , new_lockdate IN OUT DATE
  , message_description IN VARCHAR2
  ) IS
  BEGIN
    -- An account is locked only in extreme circumstances
    -- A Live account can not be locked
    -- Jackdaw interface does not allow locking
    -- To lock an account first Cancel the account and set the lockdate in the datebase
    -- A locked account can not be restored before the lockdate
    -- even if there is evidence from primay feeds. 
    -- lockdate can be set/removed/modified in the DB using Update
    IF new_live = 'Y' AND new_lockdate > SYSDATE THEN
      -- It is a cancelled account and the lockdate is in the future
      -- Account cannot be restored
      IF old_live = 'N' THEN
        raise_application_error(-20201,
          message_description ||
          ' has future lockdate ' ||
          to_char(new_lockdate, 'DD-Mon-YYYY') ||
          ', so cannot restore');
      ELSE
      -- live account should not be locked by mistake
        raise_application_error(-20201,
          message_description ||
          ' is not cancelled, so cannot set future lockdate ' ||
          to_char(new_lockdate, 'DD-Mon-YYYY'));
      END IF;
    END IF;
  END;
END;
/
