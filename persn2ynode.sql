CREATE TABLE persn2ynode
(
  crsid              VARCHAR2(8)
 ,ynode              VARCHAR2(8)
);

COMMENT ON TABLE persn2ynode IS 'This table holds ynode cohort';
ALTER TABLE persn2ynode ADD CONSTRAINT fk_persn2ynode$crsid FOREIGN KEY (crsid) REFERENCES persn (id) ENABLE;
ALTER TABLE persn2ynode ADD CONSTRAINT fk_persn2ynode$ynode FOREIGN KEY (ynode) REFERENCES ynode (id) ENABLE;
