-- Triggers

CREATE OR REPLACE TRIGGER tb_persn2srvn$persn2srvn BEFORE INSERT OR UPDATE ON persn2srvn FOR EACH ROW
BEGIN
  trg_sup.set_start_stop_date (:new.live, :old.live, :new.startdate, :new.stopdate);
  trg_sup.check_review_date(:new.live, :old.live, :new.reviewdate, 'Account for person ' || :new.persn || ' on ' || :new.srvn);
END;

/
ALTER TRIGGER tb_persn2srvn$persn2srvn ENABLE;

-- If the new srv is RAVEN then only raven_lve needs updating.
-- raven_lve default is N  
CREATE OR REPLACE TRIGGER ti_persn2srvn$persn AFTER INSERT ON persn2srvn FOR EACH ROW
BEGIN
    IF :new.srvn = 'RAVEN' THEN
      UPDATE persn
        SET raven_live = :new.live
        WHERE id = :new.persn;
    END IF;
END;
/
ALTER TRIGGER ti_persn2srvn$persn ENABLE;

-- This is very unlikely - we normally do not delete the row but set live to N and stopdate gets sysdate
CREATE OR REPLACE TRIGGER td_persn2srvn$persn AFTER DELETE ON persn2srvn FOR EACH ROW
BEGIN
    IF :old.srvn = 'RAVEN' THEN
      UPDATE persn SET raven_live = 'N' WHERE id = :old.persn;
    END IF;
END;
/
ALTER TRIGGER td_persn2srvn$persn ENABLE;

-- When updating if everything is same do not bother
-- If the service happens to be RAVEN just use new.live
CREATE OR REPLACE TRIGGER tu_persn2srvn$persn AFTER UPDATE OF persn, srvn, live ON persn2srvn FOR EACH ROW
BEGIN
  IF :new.persn = :old.persn AND :new.srvn = :old.srvn AND :new.live = :old.live THEN 
     dbms_output.put_line('all equal');
     NULL; 
  ELSE
    IF :new.srvn = 'RAVEN' THEN
      UPDATE persn
        SET raven_live = :new.live
        WHERE id = :new.persn;
     dbms_output.put_line('not equal');
    END IF;
  END IF;
END;
/
ALTER TRIGGER tu_persn2srvn$persn ENABLE;

