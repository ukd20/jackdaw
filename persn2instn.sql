-- This is to make sure that a person can multiple affiliations
-- Abode and Residence to be decided.
CREATE TABLE persn2instn
(
  crsid              VARCHAR2(8)
 ,instn              VARCHAR2(8)
 ,startdate          DATE
 ,enddate            DATE
);

COMMENT ON TABLE persn2instn IS 'This table holds affiliation.';

ALTER TABLE persn2instn ADD CONSTRAINT fk_persn2instn$crsid FOREIGN KEY (crsid) REFERENCES persn (id) ENABLE;
ALTER TABLE persn2instn ADD CONSTRAINT fk_persn2instn$instn FOREIGN KEY (instn) REFERENCES instn (id) ENABLE;
