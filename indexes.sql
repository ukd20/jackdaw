-- Indexes
--persn
CREATE INDEX i_persn$name ON persn (name);


--persn2srvn
CREATE INDEX i_persn2srvn$non_rav_pers_live ON persn2srvn (non_raven_persn_live);
CREATE INDEX i_persn2srvn$srv ON persn2srvn (srvn);
CREATE INDEX i_persn2srvn$ucspw_client ON persn2srvn (uispw_client);


