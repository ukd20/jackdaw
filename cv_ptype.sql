-- cv_type table definition

CREATE TABLE cv_ptype
(
  ptype VARCHAR2(1)
 ,ptype_desc VARCHAR2(100)
);

-- Comments
COMMENT ON TABLE cv_ptype IS 'This table holds the jackdaw Person  type eg. personal,shared,course, test etc.';

-- Constraints
ALTER TABLE cv_ptype ADD CONSTRAINT pk_cv_ptype$ptype PRIMARY KEY (ptype);
-- P : Personal, S : Shared/Group, C : Course/Conference, T : Test/Temporary, G : Guest
ALTER TABLE cv_ptype ADD CONSTRAINT ck_cv_ptype$ptype CHECK (ptype in ('P','S','C','T')) ENABLE;
