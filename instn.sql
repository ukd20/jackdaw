REM TABLE INSTN
CREATE TABLE instn 
(
  id              VARCHAR2(8)
 ,name            VARCHAR2(255)
 ,paycode         VARCHAR2(4)
 ,itype           VARCHAR2(1)
 ,live            VARCHAR2(1) DEFAULT 'Y'
 ,closed          VARCHAR2(1) DEFAULT 'N'
 ,startdate       DATE
 ,stopdate        DATE
 ,reviewdate      DATE
 ,last_event      NUMBER
 ,when            TIMESTAMP WITH LOCAL TIME ZONE
 ,who             VARCHAR2(30)
);


REM Comments
COMMENT ON TABLE  instn IS 'This table holds the jackdaw Institution.';
COMMENT ON COLUMN instn.id IS 'This is the Institution ID.';
COMMENT ON COLUMN instn.name IS 'Name of the institution';
COMMENT ON COLUMN instn.paycode IS 'To be ivestigated';
COMMENT ON COLUMN instn.itype IS 'This holds the type of ID as defined in Controlled Vocabulary Table.';
COMMENT ON COLUMN instn.live IS 'The current status of the Institution';
COMMENT ON COLUMN instn.startdate IS 'Creation Date never to be updated.';
COMMENT ON COLUMN instn.stopdate IS 'Very rarely used.';
COMMENT ON COLUMN instn.reviewdate IS 'Request to continue.';
COMMENT ON COLUMN instn.last_event IS 'Event log.';
COMMENT ON COLUMN instn.when IS 'When was the last change.';
COMMENT ON COLUMN instn.who IS 'Who changed. ';

REM Need to add address

ALTER TABLE instn ADD CONSTRAINT pk_instn$id PRIMARY KEY (id);
ALTER TABLE instn ADD CONSTRAINT ck_instn$live CHECK (live in ('Y','N')) ENABLE;
ALTER TABLE instn ADD CONSTRAINT ck_instn$closed CHECK (live in ('Y','N')) ENABLE;
ALTER TABLE instn ADD CONSTRAINT fk_instn$itype FOREIGN KEY (itype) REFERENCES cv_itype(itype);

