REM audit_table

CREATE TABLE persn_audit
(
  event               NUMBER
 ,last_event          NUMBER
 ,id                  VARCHAR2(8) 
 ,ptype               VARCHAR2(1)
 ,cuid                VARCHAR2(15) 
 ,name                VARCHAR2(255)
 ,live                VARCHAR2(1)
 ,rules_agreed        VARCHAR2(1)
 ,retired             VARCHAR2(1)
 ,lockdate            DATE
 ,when                TIMESTAMP WITH LOCAL TIME ZONE
 ,who                 VARCHAR2(30)
 ,action              VARCHAR2(12)
);

COMMENT ON COLUMN persn_audit.event IS 'Event log connected to persn.last_event.';
COMMENT ON COLUMN persn_audit.action IS 'What type of action U, D etc.';
