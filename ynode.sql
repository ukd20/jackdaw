REM TRIN16 is an exmaple of yearnode. In old jackdaw desgin it was treated as Inst. 
REM It is cohort. 
CREATE TABLE ynode
(
  id          VARCHAR2(8)
 ,instn       VARCHAR2(8)
 ,name        VARCHAR2(255)
 ,live        VARCHAR2(1) DEFAULT 'Y'
 ,closed      VARCHAR2(1) DEFAULT 'N'
 ,startdate   DATE
 ,stopdate    DATE
);

ALTER TABLE ynode ADD CONSTRAINT pk_ynode$id PRIMARY KEY (id);
ALTER TABLE ynode ADD CONSTRAINT ck_ynode$live CHECK (live in ('Y','N')) ENABLE;
ALTER TABLE ynode ADD CONSTRAINT ck_ynode$closed CHECK (live in ('Y','N')) ENABLE;
ALTER TABLE ynode ADD CONSTRAINT fk_ynode$instn FOREIGN KEY (instn) REFERENCES instn(id);

CREATE OR REPLACE TRIGGER tbiu_ynode$ynode BEFORE INSERT OR UPDATE ON ynode FOR EACH ROW
BEGIN
-- Maintains start and stop dates based on new/old Live status
  trg_sup.set_start_stop_date (:new.live, :old.live, :new.startdate, :new.stopdate);
END;
/
ALTER TRIGGER tbiu_ynode$ynode ENABLE;
