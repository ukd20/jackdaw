-- Create table 

CREATE TABLE srvn
( 
  id            VARCHAR2(12)
 ,name          VARCHAR2(255)
 ,quota         NUMBER
 ,grp_quota     NUMBER
 ,owner_is_user VARCHAR2(1) NOT NULL
);


-- Comments

COMMENT ON TABLE srvn IS 'This table holds the list of services that jackdaw administers.';
COMMENT ON COLUMN srvn.id IS 'This is the unique service id.';

-- Constraints
ALTER TABLE srvn ADD CONSTRAINT pk_srvn$id PRIMARY KEY (id);

-- Check Constraints
ALTER TABLE srvn ADD CONSTRAINT ck_srvn$owner_is_user CHECK (owner_is_user in ('Y','N')) ENABLE;

