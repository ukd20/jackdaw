-- Create Table
CREATE TABLE persn2srvn 
(
  persn                 VARCHAR2(8)
 ,srvn                  VARCHAR2(12)
 ,live                  VARCHAR2(1) DEFAULT 'N' NOT NULL
 ,startdate             DATE
 ,stopdate              DATE
 ,reviewdate            DATE
 -- These virtual columns are kept because the are very useful
 -- Saves where clause like where live = 'Y' 
 ,persn_live            VARCHAR2(8) GENERATED ALWAYS AS (CASE live WHEN 'Y' THEN persn END) VIRTUAL
 -- Saves where clause like where live = 'Y' 
 ,srvn_live             VARCHAR2(12) GENERATED ALWAYS AS (CASE live WHEN 'Y' THEN srvn END) VIRTUAL
 -- Equivalent where clause srvn <> 'RAVEN' and live = 'Y'
 ,non_raven_persn_live  VARCHAR2(8) GENERATED ALWAYS AS (CASE  WHEN (srvn <>'RAVEN' AND live ='Y') THEN persn END) VIRTUAL 
 -- Equivalent where clause srvn = 'RAVEN' and live = 'Y'
 ,raven_persn_live      VARCHAR2(8) GENERATED ALWAYS AS (CASE  WHEN (srvn='RAVEN' AND live='Y') THEN persn END) VIRTUAL 
 -- Equivalent where clause srv in ('HERMES','PWF') and live = 'Y', EXOL is not included since it does not send pw.
 ,uispw_client          VARCHAR2(8) GENERATED ALWAYS AS (CASE  WHEN ((srvn='HERMES' OR srvn='PWF') AND live='Y') THEN persn END) VIRTUAL 
);


-- Comments
COMMENT ON TABLE persn2srvn IS 'UIS registered Services for the Person';
COMMENT ON COLUMN persn2srvn.persn IS 'This is the CRSid.';
COMMENT ON COLUMN persn2srvn.srvn IS 'UIS registered service';

-- Constraints
ALTER TABLE persn2srvn ADD CONSTRAINT pk_persn2srvn$persn$srvn PRIMARY KEY (persn,srvn);
ALTER TABLE persn2srvn ADD CONSTRAINT fk_persn2srvn$persn FOREIGN KEY (persn) REFERENCES persn (id) ENABLE;
ALTER TABLE persn2srvn ADD CONSTRAINT fk_persn2srvn$srvn FOREIGN KEY (srvn) REFERENCES srvn (id) ENABLE;
-- Make sure the virtual columns are unique too
ALTER TABLE persn2srvn ADD CONSTRAINT uq_persn2srvn$persn$srvnlive UNIQUE (persn_live,srvn_live);
ALTER TABLE persn2srvn ADD CONSTRAINT uq_persn2srvn$raven_persn_live UNIQUE (raven_persn_live);

-- This will be useful when loading the data for the first time.
-- raven_id_live depends on raven_live which in turn gets updated by a trigger on persn_srvn - I need to review this
ALTER TABLE persn2srvn ADD CONSTRAINT fk_persn2srvn$raven_persn_live FOREIGN KEY (raven_persn_live) REFERENCES persn (raven_id_live)  DEFERRABLE INITIALLY DEFERRED ENABLE;
ALTER TABLE persn2srvn ADD CONSTRAINT fk_persn2srvn$non_rav_psn_live FOREIGN KEY (non_raven_persn_live) REFERENCES persn (id_live) ENABLE;
-- Raven4Life ?
ALTER TABLE persn2srvn ADD CONSTRAINT fk_persn2srvn$raven4life FOREIGN KEY (raven_persn_live) REFERENCES persn (id_live) DISABLE;

-- Check Constraints
ALTER TABLE persn2srvn ADD CONSTRAINT ck_persn2srvn$live CHECK (live in ('Y','N')) ENABLE;

