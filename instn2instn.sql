CREATE TABLE instn2instn
(
  inst1                VARCHAR2(8) -- parent inst
 ,inst2                VARCHAR2(8) -- child inst
);

COMMENT ON TABLE instn2instn IS 'This table holds the related CRSids';

REM Constraints
ALTER TABLE instn2instn ADD CONSTRAINT fk_instn2instn$inst1 FOREIGN KEY (inst1) REFERENCES instn (id) ENABLE;
ALTER TABLE instn2instn ADD CONSTRAINT fk_instn2instn$inst2 FOREIGN KEY (inst2) REFERENCES instn (id) ENABLE;

