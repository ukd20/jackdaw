REM Maintains start and stop dates based on new/old Live status

CREATE OR REPLACE TRIGGER tbiu_instn$instn BEFORE INSERT OR UPDATE ON instn FOR EACH ROW
BEGIN
  trg_sup.set_start_stop_date (:new.live, :old.live, :new.startdate, :new.stopdate);
END;
/
ALTER TRIGGER tbiu_instn$instn ENABLE;


REM Before insert set who,when and last_event 
CREATE OR REPLACE TRIGGER tbi_instn$instn BEFORE INSERT ON instn FOR EACH ROW
BEGIN
  :new.last_event := NULL;
  :new.when := systimestamp;
  :new.who := nvl(sys_context('foobar','user'),user);
END;
/
ALTER TRIGGER tbi_instn$instn ENABLE;

REM Before update of listed fields set who,when and last_event
CREATE OR REPLACE TRIGGER tbu_instn$instn BEFORE UPDATE OF 
  id
 ,name
 ,paycode
 ,itype
 ,live
 ,closed
ON instn FOR EACH ROW
BEGIN
  :new.last_event := s_log_order.nextval;
  :new.when := systimestamp;
  :new.who := nvl(sys_context('foobar','user'),user);
END;
/

ALTER TRIGGER tbu_instn$instn ENABLE;

REM After delete audit the listed fields
CREATE OR REPLACE TRIGGER tad_instn$audit AFTER DELETE ON instn FOR EACH ROW
BEGIN
  INSERT INTO instn_audit
  (
    last_event
   ,id
   ,name
   ,paycode
   ,itype
   ,live
   ,closed
   ,when
   ,who
   ,action
  )
 VALUES 
 (
    :old.last_event
   ,:old.id
   ,:old.name
   ,:old.paycode
   ,:old.itype
   ,:old.live
   ,:old.closed
   ,:old.when
   ,:old.who
   ,'D'
 );
END;
/

ALTER TRIGGER tad_instn$audit ENABLE;

REM After update audit the listed fields
CREATE OR REPLACE TRIGGER tau_instn$audit AFTER UPDATE OF 
   id
  ,name
  ,paycode
  ,itype
  ,live
  ,closed
ON instn FOR EACH ROW
BEGIN
  INSERT INTO instn_audit
  (
    event
   ,last_event
   ,id
   ,name
   ,paycode
   ,itype
   ,live
   ,closed
   ,when
   ,who
   ,action
  )
 VALUES 
 (
    :new.last_event
   ,:old.last_event
   ,:old.id
   ,:old.name
   ,:old.paycode
   ,:old.itype
   ,:old.live
   ,:old.closed
   ,:old.when
   ,:old.who
   ,'U'
 );
END;
/

ALTER TRIGGER tau_instn$audit ENABLE;

