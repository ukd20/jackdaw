REM  Table CV_ITYPE
CREATE TABLE cv_itype
(
  itype          VARCHAR2(1)
 ,itype_desc    VARCHAR2(100)
);

REM  Comments
COMMENT ON TABLE cv_itype IS 'This table holds the jackdaw Insitituiton type eg. Dept, Coll, External etc.';
COMMENT ON COLUMN cv_itype.itype IS 'Holds the type of Instituition I,D,C .';
COMMENT ON COLUMN cv_itype.itype_desc IS 'Holds the descriptions of itype for example, Institution, Department, College etc..';

REM  Constraints
ALTER TABLE cv_itype ADD CONSTRAINT pk_cv_itype PRIMARY KEY (itype);
ALTER TABLE cv_itype ADD CONSTRAINT ck_cv_itype$itype CHECK (itype in ('I','D','C')) ENABLE;
