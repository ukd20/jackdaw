CREATE TABLE instn_audit
(
  event           NUMBER
 ,last_event      NUMBER
 ,id              VARCHAR2(8)
 ,name            VARCHAR2(255)
 ,paycode         VARCHAR2(4)
 ,itype           VARCHAR2(1)
 ,live            VARCHAR2(1) DEFAULT 'Y'
 ,closed          VARCHAR2(1) DEFAULT 'N'
 ,when            TIMESTAMP WITH LOCAL TIME ZONE
 ,who             VARCHAR2(30)
 ,action          VARCHAR2(12)
);
