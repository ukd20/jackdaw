CREATE TABLE persn2persn
(
  crsid1                VARCHAR2(8) -- primary CRSid
 ,crsid2                VARCHAR2(8) -- related CRSid 
 ,ptype1                VARCHAR2(1)
 ,ptype2                VARCHAR2(1)
);

COMMENT ON TABLE persn2persn IS 'This table holds the related CRSids';

REM Constraints
ALTER TABLE persn2persn ADD CONSTRAINT fk_persn2persn$crsid1 FOREIGN KEY (crsid1) REFERENCES persn (id) ENABLE;
ALTER TABLE persn2persn ADD CONSTRAINT fk_persn2persn$crsid2 FOREIGN KEY (crsid2) REFERENCES persn (id) ENABLE;
ALTER TABLE persn2persn ADD CONSTRAINT fk_persn2persn$ptype1 FOREIGN KEY (ptype1) REFERENCES cv_ptype (ptype) ENABLE;
ALTER TABLE persn2persn ADD CONSTRAINT fk_persn2persn$ptype2 FOREIGN KEY (ptype2) REFERENCES cv_ptype (ptype) ENABLE;
ALTER TABLE persn2persn ADD CONSTRAINT ck_persn2persn$ptype12 CHECK (ptype1 = ptype2) ENABLE;
