REM Triggers

REM Before insert or update to ensure conistency.

CREATE OR REPLACE TRIGGER tbiu_persn$persn BEFORE INSERT OR UPDATE ON persn FOR EACH ROW
BEGIN
  --  Maintains start and stop dates based on new/old Live status
  trg_sup.set_start_stop_date (:new.live, :old.live, :new.startdate, :new.stopdate);
  -- Check reviewdate based on new/old Live status
  trg_sup.check_review_date(:new.live, :old.live, :new.reviewdate, 'Person ' || :new.id);
  -- Make sure that locked account can not restored before the lockdate
  -- Should not allow a live account to be locked by accident.
  trg_sup.check_lock_date(:new.live, :old.live, :new.lockdate, 'Person ' || :new.id);
END;
/

ALTER TRIGGER tbiu_persn$persn ENABLE;

REM Before insert set who,when and last_event 
CREATE OR REPLACE TRIGGER tbi_persn$persn BEFORE INSERT ON persn FOR EACH ROW
BEGIN
  :new.last_event := NULL;
  :new.when := systimestamp;
  :new.who := nvl(sys_context('foobar','user'),user);
END;
/

ALTER TRIGGER tbi_persn$persn ENABLE;

REM Before update of listed fields set who,when and last_event
CREATE OR REPLACE TRIGGER tbu_persn$persn BEFORE UPDATE OF 
   id
  ,ptype
  ,cuid
  ,name
  ,live
  ,rules_agreed
  ,retired
  ,lockdate
ON persn FOR EACH ROW
BEGIN
  :new.last_event := s_log_order.nextval;
  :new.when := systimestamp;
  :new.who := nvl(sys_context('foobar','user'),user);
END;
/

ALTER TRIGGER tbu_persn$persn ENABLE;

REM After delete audit the listed fields
CREATE OR REPLACE TRIGGER tad_persn$audit AFTER DELETE ON persn FOR EACH ROW
BEGIN
  INSERT INTO persn_audit
  (
    last_event
   ,id
   ,ptype
   ,cuid
   ,name
   ,live
   ,rules_agreed
   ,retired
   ,lockdate
   ,when
   ,who
   ,action
  )
 VALUES 
 (
    :old.last_event
   ,:old.id
   ,:old.ptype
   ,:old.cuid
   ,:old.name
   ,:old.live
   ,:old.rules_agreed
   ,:old.retired
   ,:old.lockdate
   ,:old.when
   ,:old.who
   ,'D'
 );
END;
/

ALTER TRIGGER tad_persn$audit ENABLE;

REM After update audit the listed fields
CREATE OR REPLACE TRIGGER tau_persn$audit AFTER UPDATE OF 
   id
  ,ptype
  ,cuid
  ,name
  ,live
  ,rules_agreed
  ,retired
  ,lockdate
ON persn FOR EACH ROW
BEGIN
  INSERT INTO persn_audit
  (
    event
   ,last_event
   ,id
   ,ptype
   ,cuid
   ,name
   ,live
   ,rules_agreed
   ,retired
   ,lockdate
   ,when
   ,who
   ,action
  )
 VALUES 
 (
    :new.last_event
   ,:old.last_event
   ,:old.id
   ,:old.ptype
   ,:old.cuid
   ,:old.name
   ,:old.live
   ,:old.rules_agreed
   ,:old.retired
   ,:old.lockdate
   ,:old.when
   ,:old.who
   ,'U'
 );
END;
/

ALTER TRIGGER tau_persn$audit ENABLE;
