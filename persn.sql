-- persn Table definition
-- Define all the attributes associated with person.

CREATE TABLE persn 
(
  id                  VARCHAR2(8) 
 ,ptype               VARCHAR2(1) NOT NULL
 ,owner               VARCHAR2(8) 
 ,cuid                VARCHAR2(15)  -- CUNNNNNNNNNNNND D : checkdigit
 ,name                VARCHAR2(255) -- Traditional Jackdaw Name 
 ,live                VARCHAR2(1) DEFAULT 'Y' NOT NULL
 ,startdate           DATE 
 ,stopdate            DATE
 ,reviewdate          DATE
 ,rules_agreed        VARCHAR2(1)
 ,retired             VARCHAR2(1)
 ,lockdate            DATE
 ,last_event          NUMBER
 ,when                TIMESTAMP WITH LOCAL TIME ZONE
 ,who                 VARCHAR2(30)
 -- Saves Where Clause 
 ,id_live             VARCHAR2(8) GENERATED ALWAYS AS (CASE live  WHEN 'Y' THEN id END) VIRTUAL
 -- Useful construct to be updated by trigger when RAVEN is enabled for the CRSid. Will save a join
 ,raven_live          VARCHAR2(1) DEFAULT 'N' NOT NULL
 -- Depends on raven_live. Again will save a join
 ,raven_id_live       VARCHAR2(8) GENERATED ALWAYS AS (CASE raven_live WHEN 'Y' THEN id END) VIRTUAL
);                    

-- Comments
COMMENT ON TABLE persn IS 'This table holds the jackdaw Person.';
COMMENT ON COLUMN persn.id IS 'This is the CRSid.';
COMMENT ON COLUMN persn.ptype IS 'This holds the type of ID as defined in Controlled Vocabulary Table.';
COMMENT ON COLUMN persn.owner IS 'For a group account owner has to be Valid id';
COMMENT ON COLUMN persn.cuid IS 'Place holder for future unique ID across all systems from RaaS.';
COMMENT ON COLUMN persn.name IS 'Traditional jackdaw name Surname Initial format';
COMMENT ON COLUMN persn.live IS 'The current status of CRSid ';
COMMENT ON COLUMN persn.startdate IS 'Creation Date never to be updated.';
COMMENT ON COLUMN persn.stopdate IS 'Evidence based.';
COMMENT ON COLUMN persn.reviewdate IS 'Request to continue ';
COMMENT ON COLUMN persn.rules_agreed IS 'T and C flag as updated by Raven;';
COMMENT ON COLUMN persn.retired IS 'Retired flag, updated manually.';
COMMENT ON COLUMN persn.lockdate IS 'Date on account was locked.';
COMMENT ON COLUMN persn.last_event IS 'Event log.';
COMMENT ON COLUMN persn.when IS 'When was the last change.';
COMMENT ON COLUMN persn.who IS 'Who changed. ';

-- Constraints
ALTER TABLE persn ADD CONSTRAINT pk_persn$id PRIMARY KEY (id);
ALTER TABLE persn ADD CONSTRAINT fk_persn$ptype FOREIGN KEY (ptype) REFERENCES cv_ptype (ptype) ENABLE;
ALTER TABLE persn ADD CONSTRAINT fk_persn$owner FOREIGN KEY (owner) REFERENCES persn (id) ENABLE;


ALTER TABLE persn ADD CONSTRAINT uq_persn$id_live_live UNIQUE (id_live);
ALTER TABLE persn ADD CONSTRAINT uq_persn$raven_id_live UNIQUE (raven_id_live);

-- Check Constraints
ALTER TABLE persn ADD CONSTRAINT ck_persn$live CHECK (live in ('Y','N')) ENABLE;
ALTER TABLE persn ADD CONSTRAINT ck_persn$id   CHECK (regexp_like(id, '^[A-Z][A-Z0-9]+$')) ENABLE;
ALTER TABLE persn ADD CONSTRAINT ck_persn$rules_agreed CHECK (rules_agreed = 'Y') ENABLE;
ALTER TABLE persn ADD CONSTRAINT ck_persn$name CHECK (regexp_like(name, '^[- ./''A-Za-z0-9]+$')) ENABLE;
ALTER TABLE persn ADD CONSTRAINT ck_persn$retired CHECK (retired = 'Y') ENABLE;
